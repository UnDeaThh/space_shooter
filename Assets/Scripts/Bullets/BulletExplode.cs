﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletExplode : MonoBehaviour
{
    public float speed;
    public float timeExp;
 
    private float currentTimeExp=0;

    
    // Update is called once per frame
    void Update () {
        currentTimeExp += Time.deltaTime;
        transform.Translate (speed * Time.deltaTime,0, 0);
        if(currentTimeExp>timeExp){
            Destroy (gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish" || other.tag == "meteor") {
            Destroy (gameObject);
        }
    }

}
