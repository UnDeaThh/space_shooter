﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay(){
        SceneManager.LoadScene("Save_015");
    }

    public void PulsaExit(){
        Application.Quit();
    }
}
