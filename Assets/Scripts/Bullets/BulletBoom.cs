﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBoom : MonoBehaviour
{
    public GameObject laserBoom;
    public float speed;
    public float timeBoom;
 
    private float currentTimeBoom=0;

    
    // Update is called once per frame
    void Update () {
        transform.Translate (speed * Time.deltaTime,0, 0);
        currentTimeBoom += Time.deltaTime;
        if(currentTimeBoom>timeBoom){
        Instantiate (laserBoom, this.transform.position, Quaternion.identity, null);
        GameObject go = Instantiate (laserBoom, this.transform.position, Quaternion.identity, null);
        go.transform.Rotate(0,0,30);
        GameObject go2 = Instantiate (laserBoom, this.transform.position, Quaternion.identity, null);
        go2.transform.Rotate(0,0,-30);
       
        
        currentTimeBoom = 0; 

        }
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish" || other.tag == "meteor") {
            Destroy (gameObject);
        }
    }

}
