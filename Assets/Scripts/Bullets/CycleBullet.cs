﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class CycleBullet : MonoBehaviour
{
    public float speed;
    public float timeCycle;
 
    private float currentTimeCycle=0;
 
   
    // Update is called once per frame
    void Update () {
        currentTimeCycle += Time.deltaTime;
        if(currentTimeCycle<timeCycle){
            transform.Translate (speed * Time.deltaTime,0, 0);
        }else if(currentTimeCycle>timeCycle && currentTimeCycle<timeCycle*2){
            transform.Translate (0, -speed * Time.deltaTime,0);        
        }else if(currentTimeCycle>timeCycle*2 && currentTimeCycle<timeCycle*3){
            transform.Translate (-speed * Time.deltaTime,0, 0);
        }else if(currentTimeCycle>timeCycle*3 && currentTimeCycle<timeCycle*5){
            transform.Translate (0, speed * Time.deltaTime,0);
        }else if(currentTimeCycle>timeCycle*5 && currentTimeCycle<timeCycle*6){
            transform.Translate (speed * Time.deltaTime,0, 0);
        
        }else{
            currentTimeCycle = 0;
        }
    }
 
    public void OnTriggerEnter2D(Collider2D other){
        
        if (other.tag == "Finish" || other.tag == "meteor") {
            Destroy (gameObject);
        }
    }
 
}