﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorManager : MonoBehaviour
{
   public GameObject meteorPrefab;
   public float timeLaunchMeteor;

   private float currentTime = 0;
    

    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLaunchMeteor){
            currentTime = 0;
            Instantiate(meteorPrefab,new Vector3(12,Random.Range(-7,7)),Quaternion.identity,this.transform);
        }
        
    }
}
