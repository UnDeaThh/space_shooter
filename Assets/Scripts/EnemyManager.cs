﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour
{
   public GameObject enemyPrefab;
   public float timeLaunchEnemy;

   private float currentTime = 0;
    

    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLaunchEnemy){
            currentTime = 0;
            Instantiate(enemyPrefab,new Vector3(12,Random.Range(-7,7)),Quaternion.identity,this.transform);
        }
        
    }
}