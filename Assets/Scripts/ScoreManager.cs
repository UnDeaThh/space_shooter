﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class ScoreManager : MonoBehaviour
{
    [SerializeField] GameObject[] lives;
    
    [SerializeField] Text score;
 
    private int scoreInt;

    private int numberlives;

    
 
    // Start is called before the first frame update
    void Start()
    {
        numberlives = 3;
        scoreInt = 0;
        score.text = scoreInt.ToString("000000");
       
    }
 
    public void AddScore(int value){
        scoreInt+=value;
        score.text = scoreInt.ToString("000000");
    }
   public void LoseLives(){
       numberlives--;
       for(int i=0;i<lives.Length;i++){
           lives[i].SetActive(i<numberlives);

       }
   }
    
    
}