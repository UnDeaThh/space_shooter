﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreeBullet : MonoBehaviour
{
    public GameObject laserTriple;
    public float speed;
    public float timeTrp;

    private float currentTimeTrp=0;

    
    // Update is called once per frame
    void Update () {
        transform.Translate (speed * Time.deltaTime,0, 0);
        currentTimeTrp += Time.deltaTime;
        if (currentTimeTrp>timeTrp){
        Instantiate (laserTriple, this.transform.position, Quaternion.identity, null);
        GameObject go = Instantiate (laserTriple, this.transform.position, Quaternion.identity, null);
        go.transform.Rotate(0,0,30);
        GameObject go2 = Instantiate (laserTriple, this.transform.position, Quaternion.identity, null);
        go2.transform.Rotate(0,0,-30);
        currentTimeTrp = 0;
        }
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish" || other.tag == "meteor") {
            Destroy (gameObject);
        }
    }

}
